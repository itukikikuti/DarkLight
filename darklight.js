temp()

function temp() {
    let events = []

    search(document.body, events)
    
    for (let event of events) {
        event();
    }
}

function search(node, events) {
    if (node.style !== undefined) {
        darkize(node, events)
    }
    
    let children = node.childNodes
    for (let i = 0; i < children.length; i++) {
        search(children[i], events)
    }
}

function darkize(node, events) {
    let backgroundColor = darkizeProperty(node, "background-color")
    let color = darkizeProperty(node, "color")

    events.push(() => node.style.backgroundColor = backgroundColor)
    events.push(() => node.style.color = color)
}

function darkizeProperty(node, propertyName) {
    let string = window.getComputedStyle(node, null).getPropertyValue(propertyName)
    let value = string.replace("rgba(", "").replace("rgb(", "").replace(")", "")
    let color = value.split(", ")
    let hsl = RGBToHSL(color)
    hsl[2] = 100 - hsl[2]

    let property
    if(color.length > 3) {
        property = "hsla(" + hsl[0] + ", " + hsl[1] + "%, " + hsl[2] + "%, " + color[3] + ")"
    } else {
        property = "hsl(" + hsl[0] + ", " + hsl[1] + "%, " + hsl[2] + "%)"
    }

    console.log(node.tagName + " " + string + " -> " + property)
    return property
}

function RGBToHSL(rgb) {
    const r = 0, g = 1, b = 2
    let max = Math.max.apply(null, rgb)
    let min = Math.min.apply(null, rgb)

    let h
    if (min === max) {
        h = 0
    }
    else if (min === rgb[b]) {
        h = 60 * ((rgb[g] - rgb[r]) / (max - min)) + 60
    }
    else if (min === rgb[r]) {
        h = 60 * ((rgb[b] - rgb[g]) / (max - min)) + 180
    }
    else if (min === rgb[g]) {
        h = 60 * ((rgb[r] - rgb[b]) / (max - min)) + 300
    }

    let s = max - min
    let l = (max + min) / 2 / 255 * 100

    return [h, s, l]
}
